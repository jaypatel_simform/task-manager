const request = require('supertest')
const jwt = require("jsonwebtoken")
const mongoose = require("mongoose")
const app = require('../src/app')
const User = require('../src/models/user')
const {userOne,userOneId,setupDatabase} = require("./fixtures/db")

beforeEach(setupDatabase)

afterEach(() => {
})
test('signup user', async () => {
    const res = await request(app).post("/users").send({
        name: "kay",
        email: "jay@exp.com",
        password: "jay@1228"
    }).expect(201)

    //assert that database was changed correctly
    const user = await User.findById(res.body.user._id)
    expect(user).not.toBeNull()

    //assertion about response body
    //expect(res.body.user.name).toBe("kay")
    expect(res.body).toMatchObject({
        user: {
            email: "jay@exp.com",
            name: "kay"
        }
    })
    expect(user.password).not.toBe("jay@1228")
})

test('Should login existing user', async () => {
    const res = await request(app).post('/users/login').send({
        email: "pateljaykjp@gmail.com",
        password: "mike@!@1228"
    }).expect(200)
    const user = await User.findById(userOneId)
    console.log("expe:",res.body.token,user.tokens[0].token)
    expect(res.body.token).toBe(user.tokens[1].token)
})
 
test("should not login non existing user ", async () => {
    const res = await request(app).post("/users/login").send({
        email: "ja@gmal.com",
        password: "ja12jjjaj"
    }).expect(400)
})

test("get profile for user", async () => {
    await request(app)
        .get("/users/me")
        .set("Authorization", `bear ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test("should not get profile for not autheticaed", async () => {
    await request(app)
        .get('/users/me')
        .send()
        .expect(401)
})

test("delete account for user", async () => {
    const res=await request(app)
        .delete('/users/me')
        .set("Authorization", `bear ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
        const user = await User.findById(userOneId)
        expect(user).toBeNull()
})

test("not delete account for unauthorized user", async () => {
    await request(app) 
        .delete('/users/me')
        .send()
        .expect(401)
})

test("should upload avatar image",async()=>{
     await request(app)
        .post('/users/me/avatar')
        .set("Authorization",`bear ${userOne.tokens[0].token}`)
        .attach('avatar','tests/fixtures/car.jpg')
        .expect(200)
    const user = await User.findById(userOneId)
    expect(user.avatar).toEqual(expect.any(Buffer)) 
})

test("should update valid user fields",async()=>{
    await request(app)
        .patch("/users/me")
        .set("Authorization",`bear ${userOne.tokens[0].token}`)
        .send({
            name:"jack hunter"
        })
        .expect(200)
        const user  = await User.findById(userOneId)
        expect(user.name).toEqual("jack hunter")
})

test("should not update valid user fields",async()=>{
    await request(app)
        .patch("/users/me")
        // need to check below line
        //.set("Authorization",`bear ${userOne.tokens[0].token}`)
        .send({
            location:"bharuhc",
            name:"jack hunter"
        })
        .expect(401) 
}) 
