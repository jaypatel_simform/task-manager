const mongoose = require("mongoose")
const User = require("../../src/models/user")
const Task = require("../../src/models/task")
const jwt = require("jsonwebtoken")
const { JWT_SECRETE } = require("../../src/config/testenv")
const task = require("../../src/models/task")
const userOneId = new mongoose.Types.ObjectId()
const userOne = {
    _id: userOneId,
    name: "jay",
    email: "pateljaykjp@gmail.com",
    password: "mike@!@1228",
    tokens: [{
        token: jwt.sign({ _id: userOneId }, JWT_SECRETE)
    }]
} 
const userTwoId = new mongoose.Types.ObjectId()
const userTwo = {
    _id: userTwoId,
    name: "meet",
    email: "meet12@gmail.com",
    password: "mike@!@1228",
    tokens: [{
        token: jwt.sign({ _id: userTwoId }, JWT_SECRETE)
    }]
} 

const taskOne = {
    _id:new mongoose.Types.ObjectId(),
    description:"my task one",
    completed:false,
    owner:userOneId
}
const taskTwo = {
    _id:new mongoose.Types.ObjectId(),
    description:"my task two",
    completed:true,
    owner:userOneId
}
const taskThree = {
    _id:new mongoose.Types.ObjectId(),
    description:"my task three",
    completed:false,
    owner:userTwoId
}
const setupDatabase =async ()=>{
    await User.deleteMany()
    await Task.deleteMany()
    await new User(userOne).save()
    await new User(userTwo).save()
    await new Task(taskOne).save()
    await new Task(taskTwo).save()
    await new Task(taskThree).save()
}
module.exports = {setupDatabase,userOneId , userOne,
userTwoId,userTwo,taskOne,taskThree,taskTwo}