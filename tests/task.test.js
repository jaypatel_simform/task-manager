const request =require("supertest")
const Task =  require("../src/models/task")
const app = require('../src/app')
const {userOne,userOneId,setupDatabase,
        userTwo,userTwoId,taskTwo,taskOne,taskThree} = require("./fixtures/db")
const { response } = require("../src/app")
beforeEach(setupDatabase)

test('should create task for user',async ()=>{
    const res = await request(app)
        .post("/tasks")
        .set("Authorization",`bear ${userOne.tokens[0].token}`)
        .send({
            description:"my task 1"
        })
        .expect(201)
        const task = await Task.findById(res.body._id)
        expect(task).not.toBeNull() 
        expect(task.completed).toEqual(false)   
})

test("should fetch user tasks",async()=>{
    const res = await request(app)
        .get("/tasks")
        .set("Authorization",`bear ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
        expect(res.body.length).toEqual(2)
})

test("should not delete other user tasks",async()=>{
    const res = await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .set("Authorization",`bear ${userTwo.tokens[0].token}`)
        .send()
        .expect(400)
    const task = Task.findById(taskOne._id)
    expect(task).not.toBeNull()
})
