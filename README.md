Here I have created Task-manager API to perform below operations :
    
    1) User Module : 
       - POST/ create user
       - GET/ users
       - GET/ user/:id
       - POST/ login
       - PATCH /profile update
       - GET /profile
       - POST /avatar (profile image)
       
    2) Task Module : 
       - POST/ create task
       - GET/ tasks
       - GET/ task/:id
       - PATCH /task
       
Also implement Unit testing in demo app using JEST framework for above api's to test all test cases.

    