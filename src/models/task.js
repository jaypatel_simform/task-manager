const mongooese = require("mongoose")
const validator = require("validator")
const taskSchema = new mongooese.Schema(
    {
        description: {
            type: String,
            required: true,
            trim: true
        },
        completed: {
            type: Boolean,
            default: false
        },
        owner: {
            type: mongooese.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
},{
    timestamps:true
}
)
const task = mongooese.model('Task', taskSchema)
module.exports = task