const app = require('./app')
const {PORT} = require('./config/testenv')
const p = process.env.PORT
console.log("port",p)
app.listen(PORT, () => {
    console.log("server is running on port : ", PORT)
})

// // make relationship between user and task
// const Task = require('./models/task')
// const User = require('./models/user')
// const main = async ()=>{
//     const user = await User.findById('61fba5d452712317ea6a0ea2')
//     await user.populate('tasks') //.execPopulate()
//     console.log(user.tasks)
// }
