const sgmail = require("@sendgrid/mail")
const SENDGRID_API_KEY = "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"

sgmail.setApiKey(SENDGRID_API_KEY)

const sendWelcomeMail = (email, name) => {
    sgmail.send({
        to: email,
        from: "pateljaykjp1@gmail.com",
        subject: "Thanks for joining us!",
        text: `Welcome to the app ${name} , Let me know how you get along with the app.`
    })
}

const sendCancelationMail  = (email, name) => {
    sgmail.send({
        to: email,
        from: "pateljaykjp1@gmail.com",
        subject: "Sorry to see you go!",
        text: `Goodbye ${name}, I hope to see you back sometime soon.`
    })
}
module.exports = {sendWelcomeMail , sendCancelationMail}
