const express = require("express")
const app = express()
require('./db/mongoose')
const userRouter = require('./routers/userRoute')
const taskRouter = require('./routers/taskRoute')


app.use(express.json())
app.use(userRouter)
app.use(taskRouter)

module.exports = app 